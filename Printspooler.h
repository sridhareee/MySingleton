/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Printspooler.h
 * Author: GSRIDHA2
 *
 * Created on May 14, 2018, 4:18 PM
 */

#ifndef PRINTSPOOLER_H
#define PRINTSPOOLER_H

#include <ostream>
#include <vector>

class Printspooler {
public:
    static Printspooler* getInstance() {
        if (spooler == nullptr)  
            spooler = new Printspooler();        
        return spooler;
    }
    
    void addPrint(std::string job) {
        spool.push_back(job);
    }
    
    std::vector<std::string> getJobs() {
        return this->spool;
    }
    
    
private:
    Printspooler();
    Printspooler(const Printspooler& orig);
    virtual ~Printspooler();
    Printspooler& operator= (const Printspooler &spooler) {}    
    
    static Printspooler* spooler;
    std::vector<std::string> spool;

};

#endif /* PRINTSPOOLER_H */

