/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: GSRIDHA2
 *
 * Created on May 14, 2018, 4:05 PM
 */

#include <cstdlib>
#include <iostream>
#include <ostream>

#include "Printspooler.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    Printspooler *spooler = Printspooler::getInstance();
    spooler->addPrint("manual1.pdf");
    spooler->addPrint("manual2.pdf");
    spooler->addPrint("manual3.pdf");
    
    for(auto job : spooler->getJobs()) {
        std::cout << job << std::endl;
    }
    return 0;
}

